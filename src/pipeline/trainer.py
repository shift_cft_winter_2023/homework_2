import os
import uuid

import torch
import torch.nn as nn

from config import paths
from config.config import LEARNING_RATE
from models.models_map import models
from pipeline.dataset import create_dataset
from pipeline.reports import parameters_report
from pipeline.train import train_one_epoch
from pipeline.train_config import Config
from pipeline.validate import validate
from pipeline.visualization import metric_loss_plot


def trainer(config: Config):

    train_loader = create_dataset(
        paths.TRAIN_DATA_PATH,
        config.transform_train,
        batch_size=config.train_batch_size,
        shuffle=config.suffle_train_dataset,
    )

    test_loader = create_dataset(
        paths.TEST_DATA_PATH,
        config.transform_val,
        batch_size=config.test_batch_size,
        shuffle=config.suffle_test_dataset,
    )

    model = models.get(config.model_name)

    if model is None:
        raise Exception('Model error')
    else:
        model = model()
        model.to(config.device)
        optimizer = torch.optim.AdamW(model.parameters(), lr=LEARNING_RATE)
        criterion = nn.CrossEntropyLoss()

    if config.warm:
        checkpoint = torch.load(
            os.path.join(
                paths.RESULTS_PATH,
                config.warm,
                'model.ckpt'))
        model.load_state_dict(checkpoint['model_state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        start_epoch = checkpoint['epoch']
        if start_epoch >= config.epochs:
            raise Exception('Epoch conflict')
    else:
        start_epoch = 0

    train_loss, train_accuracy = [], []
    val_loss, val_accuracy = [], []

    for epoch in range(start_epoch + 1, config.epochs + 1):
        print(f"Epoch {epoch} of {config.epochs}")
        train_epoch_loss, train_epoch_accuracy = train_one_epoch(
            model, train_loader, optimizer, criterion, config.device)

        train_loss.append(train_epoch_loss)
        train_accuracy.append(train_epoch_accuracy)

        print(
            f"Train Loss: {train_epoch_loss:.4f}, Train Acc: {train_epoch_accuracy:.2f}")

        val_epoch_loss, val_epoch_accuracy = validate(
            model, test_loader, criterion, config.device)
        print(
            f"Test Loss: {val_epoch_loss:.4f}, Test Acc: {val_epoch_accuracy:.2f}")
        val_loss.append(val_epoch_loss)
        val_accuracy.append(val_epoch_accuracy)

    _uuid = uuid.uuid1()
    os.mkdir(os.path.join(paths.RESULTS_PATH, str(_uuid)))

    config.optimizer = optimizer
    config.criterion = criterion
    parameters_report(
        _uuid,
        config,
        train_accuracy,
        train_loss,
        val_accuracy,
        val_loss)
    metric_loss_plot(
        _uuid,
        train_accuracy,
        train_loss,
        val_accuracy,
        val_loss,
        metric='accuracy',
    )

    torch.save({'epoch': epoch,
                'model_state_dict': model.state_dict(),
                'optimizer_state_dict': optimizer.state_dict(), },
               os.path.join(paths.RESULTS_PATH, str(_uuid), 'model.ckpt')
               )
