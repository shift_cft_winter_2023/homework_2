from typing import Optional, Tuple

import pandas as pd
import torch
from PIL import Image
from torch.utils.data import Dataset
from torchvision import transforms
from torchvision.transforms import Compose

from utilits.label_preprocessing import label_preprocessing


class CifarDataset(Dataset):

    def __init__(self, df: pd.DataFrame, transforms: Optional[Compose] = None):
        self.df = df
        self.transforms = transforms

    def __getitem__(self, index: int) -> Tuple[torch.Tensor, int]:
        row = self.df.iloc[index, :]
        image = Image.open(row['image'])
        label = row['int_label']
        if self.transforms:
            image = self.transforms(image)
        return image, label

    def __len__(self) -> int:
        return len(self.df)


def create_dataset(path: str, transforms: transforms.Compose,
                   batch_size: int, shuffle: bool):
    dataset = CifarDataset(label_preprocessing(path), transforms=transforms)

    loader = torch.utils.data.DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=shuffle,
        drop_last=True,
        num_workers=2,
        pin_memory=True,
    )
    return loader
