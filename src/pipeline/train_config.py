from dataclasses import dataclass
from typing import Optional

import torch
import torch.nn as nn
from torchvision import transforms

from config import config
from pipeline.augumentation import transform_train, transform_val


@dataclass
class Config:
    model_name: Optional[str] = None,
    warm: Optional[str] = None
    device: torch.device = torch.device('cpu')
    seed: int = config.SEED
    epochs: int = config.EPOCHS
    train_batch_size: int = config.TRAIN_BATCH_SIZE
    test_batch_size: int = config.TEST_BATCH_SIZE
    suffle_train_dataset: bool = config.SHUFFLE_TRAIN_DATASET
    suffle_test_dataset: bool = config.SHUFFLE_TEST_DATASET
    transform_train: transforms.Compose = transform_train
    transform_val: transforms.Compose = transform_val
    optimizer: Optional[torch.optim.Optimizer] = None
    criterion: Optional[nn.Module] = None
