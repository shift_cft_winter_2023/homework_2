import os
from typing import List
from uuid import UUID

import matplotlib.pyplot as plt

from config.paths import RESULTS_PATH


def metric_loss_plot(uuid: UUID,
                     train_metric: List[float],
                     train_loss: List[float],
                     test_metric: List[float],
                     test_loss: List[float],
                     metric: str,):
    path = os.path.join(RESULTS_PATH, str(uuid), 'graphs')
    os.mkdir(path)
    plt.figure(figsize=(10, 7))
    plt.plot(train_metric, color='blue', label=f'train {metric}')
    plt.plot(test_metric, color='red', label=f'test {metric}')
    plt.xlabel('Epochs')
    plt.ylabel(f'{metric}')
    plt.legend()
    plt.savefig(os.path.join(path, 'metric.png'))

    plt.figure(figsize=(10, 7))
    plt.plot(train_loss, color='blue', label='train loss')
    plt.plot(test_loss, color='red', label='test loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()
    plt.savefig(os.path.join(path, 'loss.png'))
