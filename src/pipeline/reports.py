import os
from typing import List
from uuid import UUID

from config.paths import RESULTS_PATH
from pipeline.train_config import Config


def parameters_report(uuid: UUID,
                      config: Config,
                      train_metric: List[float],
                      train_loss: List[float],
                      test_metric: List[float],
                      test_loss: List[float],
                      ):
    report = {
        'timestamp': uuid.time,
        'uuid': str(uuid),
        'parameters': str(config),
    }

    losses = {
        'timestamp': uuid.time,
        'uuid': str(uuid),
        'train': train_loss,
        'test': test_loss,
    }

    metrics = {
        'timestamp': uuid.time,
        'uuid': str(uuid),
        'train': train_metric,
        'test': test_metric,
    }

    with open(os.path.join(RESULTS_PATH, str(uuid), 'parametrs'), 'w') as f:
        f.write(str(report))

    with open(os.path.join(RESULTS_PATH, str(uuid), 'loss'), 'w') as f:
        f.write(str(losses))

    with open(os.path.join(RESULTS_PATH, str(uuid), 'metric'), 'w') as f:
        f.write(str(metrics))
