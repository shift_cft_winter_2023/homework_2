import torch
import torch.nn as nn
import torch.nn.functional as F

from config.config import N_CLASSES
from layers.conv_layer import Conv2D


class SimpleCNN(nn.Module):

    def __init__(self):
        super().__init__()
        self.conv1 = Conv2D(in_channels=3, out_channels=64, dropout_rate=0.5,)
        self.conv2 = Conv2D(in_channels=64, out_channels=64,)
        self.conv3 = Conv2D(in_channels=64, out_channels=128,)
        self.fc1 = nn.Linear(in_features=128, out_features=1000,)
        self.fc2 = nn.Linear(in_features=1000, out_features=N_CLASSES,)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = self.conv1(x)
        x = self.conv2(x)
        x = self.conv3(x)
        bs, _, _, _ = x.shape
        x = F.adaptive_avg_pool2d(x, 1).reshape(bs, -1)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, 0.5)
        out = self.fc2(x)
        return out
