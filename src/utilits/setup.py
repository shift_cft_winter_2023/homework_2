import os
import random

import numpy as np
import torch

from config.config import SEED


def set_seed(seed: int = SEED):
    random.seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
    torch.backends.cudnn.benchmark = True


def set_device() -> torch.device:
    return torch.device('cuda' if torch.cuda.is_available() else 'cpu')
