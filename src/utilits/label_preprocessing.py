import glob
import os

import pandas as pd
from sklearn import preprocessing

from config.config import LABELS


def label_preprocessing(path: str) -> pd.DataFrame:
    le = preprocessing.LabelEncoder()
    df = pd.DataFrame({'image': glob.glob(os.path.join(path, '*/*.png'))})
    df['label'] = df['image'].apply(lambda p: p.split('/')[-2])
    df['int_label'] = le.fit_transform(df['label'])
    return df
