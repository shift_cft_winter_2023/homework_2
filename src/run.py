from argparse import ArgumentParser

from models.models_map import models
from pipeline import trainer
from pipeline.train_config import Config
from utilits import setup

setup.set_seed()


def parse_arguments():
    args_parser = ArgumentParser()
    args_parser.add_argument(
        '--model', '-m',
        dest='model',
        help='Model from models.models_map',
        required=True,
        choices=models.keys(),
    )
    args_parser.add_argument(
        '--warm', '-w',
        dest='warm',
        help='Warm start by uuid',
        required=False,
        default=None,
    )
    return args_parser.parse_args()


if __name__ == '__main__':
    args = parse_arguments()

    config = Config()
    config.model_name = args.model
    config.device = setup.set_device()
    config.warm = args.warm

    trainer.trainer(config)
