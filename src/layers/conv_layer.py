from typing import Optional, Tuple

import torch
import torch.nn as nn
import torch.nn.functional as F


class Conv2D(nn.Module):

    def __init__(self,
                 in_channels: int,
                 out_channels: int,
                 kernel_size: int = 5,
                 paddind: int = 1,
                 pool_size: Tuple[int, int] = (3, 2,),
                 dropout_rate: Optional[float] = None,
                 ):
        super().__init__()
        self.conv = nn.Conv2d(
            in_channels=in_channels,
            out_channels=out_channels,
            kernel_size=5,
            padding=1)
        self.pool = nn.MaxPool2d(*pool_size)
        self.dropout = dropout_rate if dropout_rate else None

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        x = F.relu(self.conv(x))
        if self.dropout:
            x = F.dropout2d(x, self.dropout)
        x = self.pool(x)
        return x
